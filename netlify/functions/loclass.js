const { loclass, opt_doReaderMAC } = require("@bettse/loclass");

const csn = Buffer.from("010a0ffff7ff12e0", "hex");
const se_key = Buffer.from("D76372C291A57E6F", "hex");
const epurse = Buffer.from("feffffffffffffff", "hex");

exports.handler = async function (event, context) {
  const start = Date.now();
  const { httpMethod, body } = event;

  if (httpMethod === "POST") {
    let se = false;
    const { body: json } = event;
    try {
      const body = JSON.parse(json);
      const { log } = body;
      if (!log) {
        throw new Error("missing log");
      }

      const authData = Buffer.from(log, "hex");

      // SE Check
      if (
        Buffer.compare(authData.slice(0, 8), csn) === 0 &&
        Buffer.compare(authData.slice(8, 16), epurse) === 0
      ) {
        const nr = authData.slice(16, 20);
        const mac = authData.slice(20, 24);
        const cc_nr_p = Buffer.concat([epurse, nr]);
        const rmac = opt_doReaderMAC(cc_nr_p, se_key);
        se = Buffer.compare(mac, rmac) == 0;
      }

      if (se) {
        throw new Error("Loclass failed: SE Key");
      }

      const key = loclass(authData, authData.length);

      console.log({ key, duration: Date.now() - start });

      return {
        statusCode: 200,
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          se,
          key: key.toString("hex"),
        }),
      };
    } catch (e) {
      return {
        statusCode: 401,
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          se,
          msg: e.message,
        }),
      };
    }
  }
};
