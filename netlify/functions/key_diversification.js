const { diversifyKey } = require("@bettse/loclass");

const elite = true;

exports.handler = async function (event, context) {
  const { httpMethod, body } = event;

  if (httpMethod === "POST") {
    const { body: json } = event;
    try {
      const body = JSON.parse(json);
      const { key: keyHex, csn: csnHex } = body;
      if (!keyHex && !csnHex) {
        throw new Error("missing csn and key");
      }

      const csn = Buffer.from(csnHex, "hex");
      const key = Buffer.from(keyHex, "hex");

      const diversifiedKey = diversifyKey(csn, key, elite);

      console.log({ csn, key, diversifiedKey: diversifiedKey.toString('hex')});

      return {
        statusCode: 200,
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          key: diversifiedKey.toString("hex"),
        }),
      };
    } catch (e) {
      return {
        statusCode: 401,
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          msg: e.message,
        }),
      };
    }
  }
};
